﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ImageMagickScreenshot.Lib
{
    public static class Settings
    {
        public static readonly string BuildPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().FullName);
    }
}
