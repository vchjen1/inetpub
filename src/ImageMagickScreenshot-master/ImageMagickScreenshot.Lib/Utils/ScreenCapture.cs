// 2>nul||@goto :batch
/*
:batch
@echo off
setlocal

:: find csc.exe
set "csc="
for /r "%SystemRoot%\Microsoft.NET\Framework\" %%# in ("*csc.exe") do  set "csc=%%#"

if not exist "%csc%" (
   echo no .net framework installed
   exit /b 10
)

if not exist "%~n0.exe" (
   call %csc% /nologo /r:"Microsoft.VisualBasic.dll" /out:"%~n0.exe" "%~dpsfnx0" || (
      exit /b %errorlevel% 
   )
)
%~n0.exe %*
endlocal & exit /b %errorlevel%

*/
// reference  
// https://gallery.technet.microsoft.com/scriptcenter/eeff544a-f690-4f6b-a586-11eea6fc5eb8

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ImageMagickScreenshot.Lib.Utils
{
    /// Provides functions to capture the entire screen, or a particular window, and save it to a file.
    public class ScreenCapture
    {
        /// Creates an Image object containing a screen shot the active window
        public Image CaptureActiveWindow()
        {
            return CaptureWindow(User32.GetForegroundWindow());
        }

        /// Creates an Image object containing a screen shot of the entire desktop
        public Image CaptureScreen()
        {
            return CaptureWindow(User32.GetDesktopWindow());
        }
         
        public void CaptureActiveWindowToFile(string filename, ImageFormat format)
        {
            using (var img = CaptureActiveWindow())
            {
                img.Save(filename, format);
            }
        }

        public void CaptureScreenToFile(string filename, ImageFormat format)
        {
            using (var img = CaptureScreen())
            {
                img.Save(filename, format);
            }
        }

        /// Creates an Image object containing a screen shot of a specific window
        private static Bitmap CaptureWindow(IntPtr handle)
        {
            // get te hDC of the target window 
            var hdcSrc = User32.GetWindowDC(handle);

            // get the size 
            var windowRect = new User32.RECT();

            User32.GetWindowRect(handle, ref windowRect);

            var width = windowRect.right - windowRect.left;
            var height = windowRect.bottom - windowRect.top;

            // create a device context we can copy to 
            var hdcDest = GDI32.CreateCompatibleDC(hdcSrc);

            // create a bitmap we can copy it to, 
            // using GetDeviceCaps to get the width/height 
            var hBitmap = GDI32.CreateCompatibleBitmap(hdcSrc, width, height);

            // select the bitmap object 
            var hOld = GDI32.SelectObject(hdcDest, hBitmap);

            // bitblt over 
            GDI32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, GDI32.SRCCOPY);

            // restore selection 
            GDI32.SelectObject(hdcDest, hOld);

            // clean up 
            GDI32.DeleteDC(hdcDest);
            User32.ReleaseDC(handle, hdcSrc);

            // get a .NET image object for it 
            var bmp = Image.FromHbitmap(hBitmap);

            // free up the Bitmap object 
            GDI32.DeleteObject(hBitmap);

            return bmp;
        }

        /// Helper class containing Gdi32 API functions
        private static class GDI32
        {
            public const int SRCCOPY = 0x00CC0020; // BitBlt dwRop parameter 

            [DllImport("gdi32.dll")]
            public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop);

            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);

            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

            [DllImport("gdi32.dll")]
            public static extern bool DeleteDC(IntPtr hDC);

            [DllImport("gdi32.dll")]
            public static extern bool DeleteObject(IntPtr hObject);

            [DllImport("gdi32.dll")]
            public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        }

        /// Helper class containing User32 API functions
        private static class User32
        {
            [DllImport("user32.dll")]
            public static extern IntPtr GetDesktopWindow();

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowDC(IntPtr hWnd);

            [DllImport("user32.dll")]
            public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);

            [DllImport("user32.dll")]
            public static extern IntPtr GetForegroundWindow();

            [StructLayout(LayoutKind.Sequential)]
            public struct RECT
            {
                public readonly int left;
                public readonly int top;
                public readonly int right;
                public readonly int bottom;
            }
        }
    }
}