﻿using ImageMagick;
using ImageMagickScreenshot.Lib.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace ImageMagickScreenshot.Lib.Utils
{
    public sealed class ImageMagickExecutor
    {
        private readonly Bitmap _bmp;
          
        public ImageMagickExecutor()
        {

        }

        public ImageMagickExecutor(Bitmap bmp)
        {
            _bmp = bmp;
        }

        public string CapturedPath { get; set; }

        public void Execute()
        {
            // Read image from file
            using (var image = new MagickImage(_bmp))
            {
                //geometry for aspect
                var size = new MagickGeometry(new Percentage(100), new Percentage(100));

                // This will resize the image to a fixed size without maintaining the aspect ratio.
                // Normally an image will be resized to fit inside the specified size.
                size.IgnoreAspectRatio = true;

                //do actually fized size
                image.Resize(size);

                // Save the result
                var savedPath = Path.Combine(Settings.BuildPath,"Screenshots", Path.ChangeExtension(Path.GetRandomFileName(), ".png"));

                //do changed
                image.Write(savedPath);

                CapturedPath = savedPath;
            }
        }

    }
}
