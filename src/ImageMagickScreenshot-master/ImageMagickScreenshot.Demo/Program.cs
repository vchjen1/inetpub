﻿using ImageMagickScreenshot.Lib.Helpers;
using ImageMagickScreenshot.Lib.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using ImageMagickScreenshot.Lib;

namespace ImageMagickScreenshot.Demo
{

    internal static class Program
    {
        //[DllImport("kernel32.dll")]
        //private static extern IntPtr GetConsoleWindow();

        //[DllImport("user32.dll")]
        //private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        //private const int SW_HIDE = 0;
        //private const int SW_SHOW = 5;

        private static void Main(string[] args)
        {
            //var handle = GetConsoleWindow();

            // Hide
            //ShowWindow(handle, SW_HIDE);

            //while (true)
            //{

            var stopWatch = new Stopwatch();

            //Method#1
            stopWatch.Start();
            var desktopBmp = ScreenCaptureHelper.CaptureDesktop();
            var executor = new ImageMagickExecutor(desktopBmp);
            executor.Execute();
            stopWatch.Stop();
            Console.WriteLine("{0}({1:g}ms)", executor.CapturedPath, stopWatch.Elapsed);

            //Method#2
            stopWatch.Start();
            var capture = new ScreenCapture();
            var img = capture.CaptureScreen();
            var path = Path.Combine(Settings.BuildPath, "Screenshots",
                Path.ChangeExtension(Path.GetRandomFileName(), ".jpg"));
            img.Save(path);
            stopWatch.Stop();
            Console.WriteLine("{0}({1:g}ms)", path, stopWatch.Elapsed);

            //    Thread.Sleep(1000);
            //}

            // Show
            //ShowWindow(handle, SW_SHOW);

        }
    }
}
