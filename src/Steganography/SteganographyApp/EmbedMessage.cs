﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SteganographyApp
{
    public partial class EmbedMessage : Form
    {
        long totalChars = 0;

        public EmbedMessage()
        {
            InitializeComponent();
        }

        private void EmbedMessage_Load(object sender, EventArgs e)
        {
            totalChars = ApplicationInfo.currentPicture.MaxEncryptedBytes;
            long charsLeft = totalChars - (textBox1.Text.Length * 2);
            labelCharsLeft.Text = "Characters left: " + (charsLeft/2);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            long charsLeft = totalChars - (textBox1.Text.Length * 2);
            labelCharsLeft.Text = "Characters left: " + (charsLeft/2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ApplicationInfo.Message = textBox1.Text;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Bitmap File|*.bmp";
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                byte[] message = Utils.Encrypt(Utils.GetBytes(textBox1.Text), ApplicationInfo.Password);
                ApplicationInfo.currentPicture.WriteInteger(message.Length);
                ApplicationInfo.currentPicture.WriteInteger(0);
                ApplicationInfo.currentPicture.WriteBytes(Utils.Encrypt(Utils.GetBytes(textBox1.Text), ApplicationInfo.Password));
                Bitmap newBmp = ApplicationInfo.currentPicture.GetImage();
                newBmp.Save(saveFileDialog.FileName);
                ApplicationInfo.GenerateFilename = saveFileDialog.FileName;

                if (MessageBox.Show("Thank you. Your message was successfully embedded into a new bitmap image. Do you want to send this image by email?",
                    "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                {
                    SendEmail snd = new SendEmail();
                    snd.ShowDialog(this);
                }
                this.Close();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
