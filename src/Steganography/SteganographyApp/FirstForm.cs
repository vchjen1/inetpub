﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SteganographyApp
{
    public partial class FirstForm : Form
    {
        public FirstForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                MessageBox.Show("This program makes use of a bitmap image to hide information. Please select a bitmap image (.bmp) from your computer.",
                    "Select a Bitmap Image", MessageBoxButtons.OK, MessageBoxIcon.Information);
                OpenFileDialog openFileDlg = new OpenFileDialog();
                openFileDlg.Title = "Select a Bitmap Image";
                openFileDlg.Filter = "Bitmap Files|*.bmp";

                if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDlg.FileName.ToLower().EndsWith(".bmp"))
                    {
                        //file is valid
                        Steganograpy steg = new Steganograpy(openFileDlg.FileName);
                        ApplicationInfo.currentPicture = steg;
                        ApplicationInfo.EmbedMessage = true;
                        ApplicationInfo.Hide = true;
                        EnterPassword pwd = new EnterPassword();
                        this.Hide();
                        pwd.ShowDialog(this);
                        this.Show();
                    }
                    else
                    {
                        MessageBox.Show("This program only works with bitmap pictures. Please select a bitmap image (.bmp) from your computer.",
                            "Select a Bitmap Image", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a bitmap image (.bmp) from your computer which contains the secret information.",
                    "Select a Bitmap Image", MessageBoxButtons.OK, MessageBoxIcon.Information);
                OpenFileDialog openFileDlg = new OpenFileDialog();
                openFileDlg.Title = "Select a Bitmap Image";
                openFileDlg.Filter = "Bitmap Files|*.bmp";

                if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDlg.FileName.ToLower().EndsWith(".bmp"))
                    {
                        //file is valid
                        Steganograpy steg = new Steganograpy(openFileDlg.FileName);
                        ApplicationInfo.currentPicture = steg;
                        ApplicationInfo.Hide = false;
                        EnterPassword pwd = new EnterPassword();
                        this.Hide();
                        pwd.ShowDialog(this);
                        this.Show();
                    }
                    else
                    {
                        MessageBox.Show("This program only works with bitmap pictures. Please select a bitmap image (.bmp) from your computer.",
                            "Select a Bitmap Image", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
    }
}
