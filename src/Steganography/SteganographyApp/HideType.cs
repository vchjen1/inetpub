﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SteganographyApp
{
    public partial class HideType : Form
    {
        public HideType()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ApplicationInfo.EmbedMessage = radioButton1.Checked;
            if (ApplicationInfo.EmbedMessage)
            {
                EmbedMessage embed = new EmbedMessage();
                embed.ShowDialog(this);
                this.Close();
            }
            else
            {
                //select file
                OpenFileDialog openDlg = new OpenFileDialog();
                openDlg.Filter = "All Files|*.*";

                int fileSize = 0;
                byte[] fileBytes = null;
                byte[] encryptedBytes = null;
                do
                {
                    DialogResult result = MessageBox.Show("Please select any file bitmap image (.bmp) from your computer which contains the secret information. "
                    + " The maximum file size allowed is: " + ApplicationInfo.currentPicture.MaxEncryptedBytes.ToString() + " bytes.",
                        "Select a Bitmap Image", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        if (openDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {                       
                            fileBytes = System.IO.File.ReadAllBytes(openDlg.FileName);
                            fileSize = fileBytes.Length;
                            if (fileSize > ApplicationInfo.currentPicture.MaxEncryptedBytes)
                            {
                                MessageBox.Show("Sorry, the file you selected is " + fileBytes.Length.ToString() + " bytes. Maximum allowed number of bytes is: " +
                                    ApplicationInfo.currentPicture.MaxEncryptedBytes.ToString() + ". Please select another file.");
                            }
                        }
                    }
                    else
                    {
                        this.Close();
                    }
                
                } while (fileSize > ApplicationInfo.currentPicture.MaxEncryptedBytes);

                //embed file
                encryptedBytes = Utils.Encrypt(fileBytes, ApplicationInfo.Password);

                //save generated image
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Bitmap File|*.bmp";
                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ApplicationInfo.GenerateFilename = saveFileDialog.FileName;
                    byte[] filenameBytes = Utils.GetBytes(System.IO.Path.GetFileName(openDlg.FileName));
                    ApplicationInfo.currentPicture.WriteInteger(encryptedBytes.Length);
                    ApplicationInfo.currentPicture.WriteInteger(filenameBytes.Length);
                    ApplicationInfo.currentPicture.WriteBytes(filenameBytes);
                    ApplicationInfo.currentPicture.WriteBytes(encryptedBytes);
                    Bitmap newBmp = ApplicationInfo.currentPicture.GetImage();
                    newBmp.Save(saveFileDialog.FileName);
                    if (MessageBox.Show("Thank you. Your message was successfully embedded into a new bitmap image. Do you want to send this image by email?",
                        "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        SendEmail snd = new SendEmail();
                        snd.ShowDialog(this);
                    }
                    this.Close();
                }
            }
        }
    }
}
