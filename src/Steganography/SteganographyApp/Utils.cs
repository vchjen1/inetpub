﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace SteganographyApp
{
    public static class Utils
    {
        public static string ConvertToBinary(int i)
        {
            int current = i;

            string binaryResult = "";

            while (current > 0)
            {
                binaryResult = (current % 2).ToString() + binaryResult;
                current = current / 2;
            }

            if (binaryResult.Length < 32)
            {
                int difference = 32 - binaryResult.Length;
                for (int count = 0; count < difference; count++)
                    binaryResult = "0" + binaryResult;
            }

            return binaryResult;
        }

        public static string ConvertToBinary(byte i)
        {
            int current = i;
            
            string binaryResult = "";

            while (current > 0)
            {
                binaryResult = (current % 2).ToString() + binaryResult;
                current = current / 2;
            }

            if (binaryResult.Length < 8)
            {
                int difference = 8 - binaryResult.Length;
                for (int count = 0; count < difference; count++)
                    binaryResult = "0" + binaryResult;
            }

            return binaryResult;
        }

        public static int ConvertToInteger(string binary)
        {
            int returnValue = 0;
            int powerOf2 = 1;
            int currentValue = 0;
            for (int i = binary.Length - 1; i >= 0; i--)
            {
                currentValue = Int32.Parse(binary[i].ToString());
                returnValue += currentValue * powerOf2;
                powerOf2 *= 2;
            }

            return returnValue;
        }

        #region Encryption code obtained from the net

        public static string CreateMD5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();
        }

        /// <summary>
        /// AES Encryption
        /// </summary>
        public static byte[] Encrypt(byte[] src, string password)
        {
            password = CreateMD5Hash(password);
            string AesIV = password.Substring(0, 16);
            string AesKey = password.Substring(15, 16);

            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = System.Text.Encoding.UTF8.GetBytes(AesIV);
            aes.Key = System.Text.Encoding.UTF8.GetBytes(AesKey);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // encryption
            using (ICryptoTransform enc = aes.CreateEncryptor())
            {
                byte[] dest = enc.TransformFinalBlock(src, 0, src.Length);

                // Convert byte array to Base64 strings
                return dest;
            }
        }

        /// <summary>
        /// AES decryption
        /// </summary>
        public static byte[] Decrypt(byte[] src, string password)
        {
            password = CreateMD5Hash(password);
            string AesIV = password.Substring(0, 16);
            string AesKey = password.Substring(15, 16);

            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = System.Text.Encoding.UTF8.GetBytes(AesIV);
            aes.Key = System.Text.Encoding.UTF8.GetBytes(AesKey);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // decryption
            using (ICryptoTransform dec = aes.CreateDecryptor())
            {
                byte[] dest = dec.TransformFinalBlock(src, 0, src.Length);
                return dest;
            }
        }

        #endregion


        public static byte[] GetBytes(string str)
        {
            return Encoding.Default.GetBytes(str);
        }

        public static string GetString(byte[] bytes)
        {
            return Encoding.Default.GetString(bytes);
        }

        public static void SendEmail(string fromAddr, string pwd, string toAddr, string subject, string body, string bitmapAttachment)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                message.From = new MailAddress(fromAddr);
                message.To.Add(new MailAddress(toAddr));
                message.Subject = subject;
                message.Body = body;
                message.Attachments.Add(new Attachment(bitmapAttachment));

                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(fromAddr, pwd);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("err: " + ex.Message);
            }
        }

    }
}
