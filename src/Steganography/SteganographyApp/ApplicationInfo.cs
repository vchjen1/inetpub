﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SteganographyApp
{
    public class ApplicationInfo
    {
        public static Steganograpy currentPicture;
        public static bool EmbedMessage;
        public static bool Hide;
        public static string Message;
        public static string Filename;
        public static string Password;
        public static string GenerateFilename;
    }
}
