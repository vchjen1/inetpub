﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SteganographyApp
{
    public class Steganograpy
    {
        private System.Drawing.Bitmap bmp;  //bitmap file in memory
        private int width;                  //width of image
        private int height;                 //height of image
        private string filename;            //filename
        private long totalAvailable;        //total space available to store secret information
        private StringBuilder builder;      //to hold the image info in binary
        private int cursor;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filename">Filename of bitmap</param>
        public Steganograpy(string filename)
        {
            this.filename = filename;
            this.bmp = new Bitmap(Image.FromFile(filename));
            this.width = bmp.Width;
            this.height = bmp.Height;
            this.totalAvailable = (((width * height) * 3) / 8);
            toBinaryString();
            ResetCursor();
        }

        /// <summary>
        /// Maximum number of bytes that can fit into image
        /// </summary>
        public long MaxEncryptedBytes
        {
            get
            {
                return (((totalAvailable / 16) - 1) * 16) - 8 - 64;
            }
        }

        public long MaxBytes
        {
            get
            {
                return (totalAvailable / 8) - 8 - 64;
            }

        }

        /// <summary>
        /// Convert image RGB into binary
        /// </summary>
        private void toBinaryString()
        {
            builder = new StringBuilder(width * height * 8);
            for (int i = 0; i < bmp.Height; i++)
            {
                for (int j = 0; j < bmp.Width; j++)
                {
                    Color col = bmp.GetPixel(j, i);
                    builder.Append(Utils.ConvertToBinary(col.R));
                    builder.Append(Utils.ConvertToBinary(col.G));
                    builder.Append(Utils.ConvertToBinary(col.B));
                }
            }
        }

        /// <summary>
        /// gets the image file in binary format
        /// </summary>
        public string BinaryInfo
        {
            get
            {
                return builder.ToString();
            }
        }

        /// <summary>
        /// returns the manipulated image
        /// </summary>
        /// <returns>bitmap image with modified pixels</returns>
        public Bitmap GetImage()
        {
            string binaryTemp = "";
            int previousCursor = this.cursor;
            cursor = 0;
            Bitmap newBitmap = new Bitmap(bmp.Width, bmp.Height);

            for (int i = 0; i < bmp.Height; i++)
            {
                for (int j = 0; j < bmp.Width; j++)
                {
                    //read red 
                    for (int z = 0; z < 8; z++, cursor++)
                    {
                        binaryTemp += builder[cursor];
                    }
                    int r = Utils.ConvertToInteger(binaryTemp);
                    binaryTemp = "";

                    //read green 
                    for (int z = 0; z < 8; z++, cursor++)
                    {
                        binaryTemp += builder[cursor];
                    }
                    int g = Utils.ConvertToInteger(binaryTemp);
                    binaryTemp = "";

                    //read blue
                    for (int z = 0; z < 8; z++, cursor++)
                    {
                        binaryTemp += builder[cursor];
                    }
                    int b = Utils.ConvertToInteger(binaryTemp);
                    binaryTemp = "";

                    Color col = Color.FromArgb(r, g, b);
                    newBitmap.SetPixel(j, i, col);
                }
            }

            cursor = previousCursor;
            return newBitmap;
        }

        /// <summary>
        /// Set cursor to initial position
        /// </summary>
        public void ResetCursor()
        {
            cursor = 7;
        }

        /// <summary>
        /// Move cursor to next binary position
        /// </summary>
        private void moveCursor()
        {
            cursor += 8;
        }

        /// <summary>
        /// Write a single byte to the binary string
        /// </summary>
        /// <param name="b">Byte to write</param>
        private void writeByte(byte b)
        {
            string binaryInfo = Utils.ConvertToBinary(b);
            for (int i = 0; i < 8; i++)
            {
                builder[cursor] = binaryInfo[i];
                cursor += 8;
            }
        }

        /// <summary>
        /// Read a single byte from the binary string
        /// </summary>
        /// <returns></returns>
        private byte readByte()
        {
            string binaryInfo = "";
            for (int i = 0; i < 8; i++)
            {
                binaryInfo += builder[cursor];
                cursor += 8;
            }

            return (byte)Utils.ConvertToInteger(binaryInfo);
        }

        /// <summary>
        /// Writes a sequence of bytes to the stream
        /// </summary>
        /// <param name="bytes">Bytes to write</param>
        public void WriteBytes(byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; i++)
            {
                writeByte(bytes[i]);
            }
        }

        /// <summary>
        /// Reads a sequence of bytes into a byte array
        /// </summary>
        /// <param name="number">Number of bytes to read</param>
        /// <returns>An array of bytes</returns>
        public byte[] ReadBytes(int number)
        {
            byte[] data = new byte[number];
            for (int i = 0; i < number; i++)
            {
                data[i] = readByte();
            }
            return data;
        }

        /// <summary>
        /// Write an integer to the binary string
        /// </summary>
        /// <param name="i"></param>
        public void WriteInteger(int integer)
        {
            string binaryInfo = Utils.ConvertToBinary(integer);
            for (int i = 0; i < 32; i++)
            {
                builder[cursor] = binaryInfo[i];
                cursor += 8;
            }
        }

        /// <summary>
        /// Reads the binary string and extracts an integer from it
        /// </summary>
        /// <returns></returns>
        public int ReadInteger()
        {
            string binaryInfo = "";
            for (int i = 0; i < 32; i++)
            {
                binaryInfo += builder[cursor];
                cursor += 8;
            }
            //return 0;
            return Utils.ConvertToInteger(binaryInfo);
        }

        /// <summary>
        /// destructor
        /// </summary>
        ~Steganograpy()
        {
            this.bmp.Dispose();
        }

        
        
    }
}
