﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SteganographyApp
{
    public partial class SendEmail : Form
    {
        public SendEmail()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Utils.SendEmail(textBoxEmail.Text, textBoxPassword.Text, textBoxRecipientemail.Text, textBoxSubject.Text, textBoxBody.Text, ApplicationInfo.GenerateFilename);
            System.Windows.Forms.MessageBox.Show("Message was sent. Thank you.");
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SendEmail_Load(object sender, EventArgs e)
        {

        }
    }
}
