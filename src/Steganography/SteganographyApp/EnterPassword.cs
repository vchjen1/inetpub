﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SteganographyApp
{
    public partial class EnterPassword : Form
    {
        public EnterPassword()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                textBox1.PasswordChar = '*';
            }
            else
            {
                textBox1.PasswordChar = (char)0;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 5)
            {
                MessageBox.Show("Sorry, your password is too short. Please try again.");
            }
            else
            {
                if (ApplicationInfo.Hide)
                {
                    ApplicationInfo.Password = textBox1.Text;
                    HideType hide = new HideType();
                    this.Hide();
                    hide.ShowDialog(this);
                    this.Close();
                }
                else
                {
                    ApplicationInfo.Password = textBox1.Text;
                    int msgLen = ApplicationInfo.currentPicture.ReadInteger();
                    int filenameLen = ApplicationInfo.currentPicture.ReadInteger();

                    if (filenameLen > 0)
                    {
                        byte[] filenameData = ApplicationInfo.currentPicture.ReadBytes(filenameLen);
                        string filename = Utils.GetString(filenameData);
                        byte[] encryptedData = ApplicationInfo.currentPicture.ReadBytes(msgLen);
                        byte[] data = Utils.Decrypt(encryptedData, ApplicationInfo.Password);

                        SaveFileDialog saveDlg = new SaveFileDialog();
                        saveDlg.FileName = filename;
                        saveDlg.Filter = filename + "|" + filename + "|*.*|All Files";

                        if (saveDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            System.IO.FileStream streamWriter = new System.IO.FileStream(saveDlg.FileName, System.IO.FileMode.CreateNew);
                            streamWriter.Write(data, 0, data.Length);
                            streamWriter.Close();
                            MessageBox.Show("Thank you. Your information was successfully extracted.");
                            this.Close();
                        }
                    }
                    else
                    {
                        byte[] encryptedData = ApplicationInfo.currentPicture.ReadBytes(msgLen);
                        byte[] data = Utils.Decrypt(encryptedData, ApplicationInfo.Password);

                        SaveFileDialog saveDlg = new SaveFileDialog();
                        saveDlg.Filter = "Text Files|*.txt|*.*|All Files";
                        if (saveDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            System.IO.StreamWriter writer = new System.IO.StreamWriter(saveDlg.FileName);
                            writer.Write(Utils.GetString(data));
                            writer.Close();
                            MessageBox.Show("Thank you. Your information was successfully extracted.");
                            this.Close();
                        }
                    }
                }
            }
        }
    }
}
