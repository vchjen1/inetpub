﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SteganographyApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //load bitmap
        //foreach pixel {
        //  convert to binary
        //  save size of message
        //  save message
        // }

        private void button1_Click(object sender, EventArgs e)
        {
            //System.Text.StringBuilder builder = new StringBuilder();
            //if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    if (System.IO.Path.GetExtension(openFileDialog1.FileName) == ".bmp")
            //    {
            //        System.Drawing.Bitmap bmp = new Bitmap(Image.FromFile(openFileDialog1.FileName));
            //        for (int i = 0; i < bmp.Height; i++)
            //        {
            //            for (int j = 0; j < bmp.Width; j++)
            //            {
            //                Color col = bmp.GetPixel(j, i);
            //                builder.Append(col.R.ToString() + " " + col.G.ToString() + " " + col.B.ToString());
            //            }
            //            builder.Append("\r\n");
            //        }
            //    }
            //    textBox1.Text = builder.ToString();
            //}

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (System.IO.Path.GetExtension(openFileDialog1.FileName) == ".bmp")
                {
                    System.Drawing.Bitmap bmp = new Bitmap(Image.FromFile(openFileDialog1.FileName));
                    Steganograpy steg = new Steganograpy(openFileDialog1.FileName);
                    System.IO.StreamWriter writer1 = new System.IO.StreamWriter("e:\\steg1.txt");
                    writer1.Write(steg.BinaryInfo);
                    writer1.Close();

                    steg.WriteInteger(15046085);
                    steg.WriteInteger(199);
                    System.IO.StreamWriter writer2 = new System.IO.StreamWriter("e:\\steg2.txt");
                    writer2.Write(steg.BinaryInfo);
                    writer2.Close();

                    steg.GetImage().Save(@"e:\test2.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = Utils.ConvertToBinary(Int32.Parse(textBox2.Text));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int initialLength = Utils.GetBytes(textBox3.Text).Length;
            int encryptedLength = Utils.Encrypt(Utils.GetBytes(textBox3.Text), "123456").Length;
            textBox3.Text = "Initial: " + initialLength.ToString() + " Encrypted: " + encryptedLength.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox4.Text = Utils.ConvertToInteger(textBox4.Text).ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Steganograpy steg = new Steganograpy("e:\\test2.bmp");
            MessageBox.Show(steg.ReadInteger().ToString());
            MessageBox.Show(steg.ReadInteger().ToString());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (System.IO.Path.GetExtension(openFileDialog1.FileName) == ".bmp")
                {
                    Steganograpy steg = new Steganograpy(openFileDialog1.FileName);

                    if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        byte[] fileData = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                        if (fileData.Length < steg.MaxBytes)
                        {
                            string filename = System.IO.Path.GetFileName(openFileDialog1.FileName);
                            steg.WriteInteger(fileData.Length);
                            steg.WriteInteger(filename.Length);
                            steg.WriteBytes(Utils.GetBytes(filename));
                            steg.WriteBytes(fileData);
                            steg.GetImage().Save("e:\\result.bmp");
                        }
                        else
                        {
                            MessageBox.Show("File is too large. Max allowed bytes for this file : " + steg.MaxBytes);
                        }
                    }

                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (System.IO.Path.GetExtension(openFileDialog1.FileName) == ".bmp")
                {
                    Steganograpy steg = new Steganograpy(openFileDialog1.FileName);
                    int fileSize = steg.ReadInteger();
                    int filenameSize = steg.ReadInteger();

                    if (filenameSize > 0)
                    {
                        MessageBox.Show(filenameSize.ToString());
                        string filename = "E:\\" + Utils.GetString(steg.ReadBytes(filenameSize));
                        System.IO.FileStream writeStream = new System.IO.FileStream(filename, System.IO.FileMode.CreateNew);
                        byte[] data = steg.ReadBytes(fileSize);
                        writeStream.Write(data, 0, data.Length);
                        writeStream.Close();
                    }

                    //if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    //{
                    //    byte[] fileData = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                    //    if (fileData.Length < steg.MaxBytes)
                    //    {
                    //        string filename = System.IO.Path.GetFileName(openFileDialog1.FileName);
                    //        steg.WriteInteger(fileData.Length);
                    //        steg.WriteInteger(filename.Length);
                    //        steg.WriteBytes(Utils.GetBytes(filename));
                    //        steg.WriteBytes(fileData);
                    //        steg.GetImage().Save("e:\\result.bmp");
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("File is too large. Max allowed bytes for this file : " + steg.MaxBytes);
                    //    }
                    //}

                }
            }
        }
    }
}
