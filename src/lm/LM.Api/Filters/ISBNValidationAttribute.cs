﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using LM.Api.Helpers;

namespace LM.Api.Filters
{
    public class ISBNValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return IsbnHelper.TryValidate(value.ToString())
                ? ValidationResult.Success
                : new ValidationResult("Please enter a valid Isbn");
        }
    }
}