﻿(function () {

    "use strict";

    var app = angular.module("app");

    //controllers
    app.controller("booklistController", booklistCtrl);

    //injections
    booklistCtrl.$inject = ["$rootScope", "$scope", "$stateParams", "$window", "bookService", "utilityService", "bootstrap3ElementModifier"];

    function booklistCtrl($rootScope, $scope, $stateParams, $window, bookService, utilityService, bootstrap3ElementModifier) {

        bootstrap3ElementModifier.enableValidationStateIcons(false);

        $scope.page = $stateParams.page;
        $scope.search = $stateParams.search;
        $scope.searching = searching;
        $scope.downloadUrl = utilityService.getInstance().baseAddress + "/api/file/download";
        $scope.list = list;
        $scope.sorting = sorting;
        $scope.reset = reset;
        $scope.sort = $stateParams.sort;
        $scope.column = $stateParams.column;

        // initialize your users data
        (function () {

            $rootScope.title = "Books";

            list();

        })();

        function reset() {
            $scope.search = "";

            list();
        }

        function searching() {

            $window.location.href = "#/book/list?page=" + $scope.page + "&search=" + $scope.search + "&column=" + $scope.column + "&sort=" + $scope.sort;
        }

        function sorting(column) {

            $scope.column = column;

            if ($scope.sort === "asc") {
                $scope.sort = "desc";
            } else {
                $scope.sort = "asc";
            }
        }

        function list() {
            bookService.list($scope.page, $scope.search, $scope.column, $scope.sort).then(function (response) {

                $scope.bookData = response.data.items;
                $scope.pager = response.data.pager;

            })
            .catch(function (response) {
                $rootScope.error = utilityService.throwErrors(response);
            });
        }
    }

})();
