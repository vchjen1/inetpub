﻿using System;
using System.Globalization;
using System.Threading;
using NUnit.Framework;
using VendingMachine.Lib;
using VendingMachine.Lib.Exceptions;
using VendingMachine.Lib.Models;

namespace VendingMachine.NUnit
{
    [TestFixture]
    public abstract class BaseTest
    { 
        static BaseTest()
        {
            //set up UK time zone
            var culture = new CultureInfo("en-GB");
            Thread.CurrentThread.CurrentCulture = culture;
        }

        /// <summary>
        ///     Cached product items
        /// </summary>
        protected static readonly Product[] Products =
        {
            new Product
            {
                Avaliable = 3,
                Name = "Cola",
                Price = 2.1m
            },
            new Product
            {
                Avaliable = 2,
                Name = "Pepsi",
                Price = 1.8m
            }
        };

        /// <summary>
        ///     choosed Machine manificturer
        /// </summary>
        protected BaseTest(string manifacturer)
        {
            MachineLogic = new VendingMachineLogic(manifacturer)
            {
                Products = Products
            };
        }

        /// <summary>
        /// generic logic
        /// </summary>
        protected static VendingMachineLogic MachineLogic;
         

        /// <summary>
        /// logs
        /// </summary>
        /// <param name="s"></param>
        /// <param name="objects"></param>
        protected void Log(string s, params object[] objects)
        {
            Console.WriteLine(s, objects);
        }


        /// <summary>
        /// try  logic
        /// </summary>
        /// <param name="action"></param>
        protected void Try(Action action)
        {
            try
            {
                action();
            }
            catch (VendingMachineException exception)
            {
                Log(exception.Message);
            }
        }
    }
}