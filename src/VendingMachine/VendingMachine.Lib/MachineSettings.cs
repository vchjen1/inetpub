using VendingMachine.Lib.Consts;

namespace VendingMachine.Lib
{
    public static class MachineSettings
    {
        public static readonly decimal[] Coins =
        {
            CoinFollowing.Coint05Pence,
            CoinFollowing.Coint10Pence,
            CoinFollowing.Coint20Pence,
            CoinFollowing.Coint50Pence,
            CoinFollowing.Coint1Pound,
            CoinFollowing.Coint2Pound
        };

        public static decimal Calculate(int[] coins, int change)
        {
            var counts = new int[change + 1];

            counts[0] = 0;

            for (var i = 1; i <= change; i++)
            {
                var count = int.MaxValue;
                foreach (var coin in coins)
                {
                    var total = i - coin;
                    if (total >= 0 && count > counts[total])
                        count = counts[total];
                }
                counts[i] = count < int.MaxValue ? count + 1 : int.MaxValue;
            }
            return counts[change];
        }
    }
}