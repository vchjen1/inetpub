﻿namespace VendingMachine.Lib.Consts
{
    public struct CoinFollowing
    {
        public const decimal Coint05Pence = 0.05m;
        public const decimal Coint10Pence = 0.10m;
        public const decimal Coint20Pence = 0.20m;
        public const decimal Coint50Pence = 0.50m;
        public const decimal Coint1Pound = 1m;
        public const decimal Coint2Pound = 2m;
    }
}
