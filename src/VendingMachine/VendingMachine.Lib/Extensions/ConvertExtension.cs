﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendingMachine.Lib.Consts;
using VendingMachine.Lib.Models;

namespace VendingMachine.Lib.Extensions
{
    public static class ConvertExtension
    {
        public static bool ValidateMoney(this decimal d)
        {
            return CoinFollowing.Coint05Pence == d ||
                   CoinFollowing.Coint10Pence == d ||
                   CoinFollowing.Coint20Pence == d ||
                   CoinFollowing.Coint50Pence == d ||
                   CoinFollowing.Coint1Pound == d ||
                   CoinFollowing.Coint2Pound == d;
        }

    }
}
